<?php
// Set smart Directory Separator
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

// Include helper and core functions
require_once('Helper.php');
require_once('functions.php');

// Include all core classes
require_once('Session.php');
require_once('Database.php');
require_once('View.php');

// Include all controllers
foreach (glob(file_path('controllers', '*.php')) as $file) {
    include_once($file);
}

// Include all models
foreach (glob(file_path('models', '*.php')) as $file) {
    include_once($file);
}

// Execute routes
require_once('Route.php');
