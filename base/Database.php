<?php

namespace Imgnd\Arti;

use PDO;
use PDOStatement;
use PDOException;

class Database
{
    /**
     * @var PDO $db Database connection
     */
    private static PDO $db;

    /**
     * Database constructor
     */
    public function __construct()
    {
        if (!empty(env('DB_CONNECTION'))) {
            try {
                self::$db = new PDO(
                    sprintf("%s:host=%s;dbname=%s",
                        env('DB_CONNECTION'),
                        env('DB_HOST'),
                        env('DB_DATABASE')
                    ),
                    env('DB_USERNAME'),
                    env('DB_PASSWORD')
                );
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Check of Database connection is succesfull
     * @return bool
     */
    public static function check()
    {
        return isset(self::$db);
    }

    /**
     * Set table name
     * @param string $table
     * @return string
     */
    private static function table(string $table): string
    {
        if (substr($table, 0, 1) !== '`') {
            $table = "`$table` ";
        }
        return $table;
    }

    /**
     * Set columns, create string from array
     * @param string|array $columns
     * @return string
     */
    private static function columns($columns): string
    {
        if (is_array($columns)) {
            $columns = '`' . implode('`, `', $columns) . '`';
        }
        return $columns;
    }

    /**
     * Map columns with values for update query
     * @param string|array $data
     * @return string
     */
    private static function map($data): string
    {
        if (is_array($data)) {
            $return = [];
            foreach ($data as $key => $val) {
                $return[] = '`' . $key . '` = \'' . $val . '\'';
            }
            $data = implode(', ', $return);
        }
        return $data;
    }

    /**
     * Set values, create string from array
     * @param string| array $values
     * @return string
     */
    private static function values($values): string
    {
        if (is_array($values)) {
            $values = "'" . implode("', '", $values) . "'";
        }
        return $values;
    }

    /**
     * Set where clause, create string from array
     * @param string|array $where
     * @return string
     */
    private static function where($where): string
    {
        if (is_array($where)) {
            $filters = [];
            foreach ($where as $column => $value) {
                $filters[] = "`$column` = '$value'";
            }
            $where = implode(' ', $filters);
        }
        return $where;
    }

    /**
     * Execute a query and get the result
     * @param string $query
     * @param string $return
     * @return bool|PDOStatement
     */
    public static function query(string $query, string $return = 'result')
    {
        $sth = self::$db->prepare($query);
        $result = $sth->execute();
        if ($return === 'sth') {
            return $sth;
        }
        return $result;
    }

    /**
     * Insert a record to the database
     * @param string $table
     * @param string|array $columns
     * @param string|array $values
     * @return bool
     */
    public static function add(string $table, $columns = null, $values = []): bool
    {
        $query = sprintf('INSERT INTO %1$s%2$s VALUES (%3$s)',
            self::table($table),
            !empty($columns) ? '(' . self::columns($columns) . ')' : null,
            self::values($values)
        );
        return self::query($query);
    }

    /**
     * Read a table from the database
     * @param string $table
     * @param string|array $columns
     * @param string|array $where
     * @return array
     */
    public static function get(string $table, $columns = '*', $where = '1'): array
    {
        $rows = [];
        $query = sprintf('SELECT %2$s FROM %1$s WHERE %3$s',
            self::table($table),
            self::columns($columns),
            self::where($where)
        );
        $sth = self::query($query, 'sth');
        while ($row = $sth->fetch(PDO::FETCH_OBJ)) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Update database records
     * @param string $table
     * @param string|array $set
     * @param string|array $where
     * @return bool
     */
    public static function set(string $table, $set, $where = '1'): bool
    {
        $query = sprintf('UPDATE %1$s SET %2$s WHERE %3$s',
            self::table($table),
            self::map($set),
            self::where($where)
        );
        return self::query($query);
    }

    /**
     * Delete database records
     * @param string $table
     * @param string|array $where
     * @return bool
     */
    public static function del(string $table, $where): bool
    {
        $query = sprintf('DELETE FROM %1$s WHERE %2$s',
            self::table($table),
            self::where($where)
        );
        return self::query($query);
    }
}

/** Initialise Database class and create shortcut alias */
new Database();
class_alias('Imgnd\Arti\Database', 'Imgnd\Arti\DB');
