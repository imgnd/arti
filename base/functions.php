<?php
/**
 * Create direct accessible functions for Helper methods
 */
use Imgnd\Arti\Helper;

/** Set environment variables **/
Helper::assign_env();

/**
 * Echo a var_dump in nice pre tags
 * @param ...$var
 */
function dump(...$var): void
{
    echo Helper::pre_dump(...$var);
}

/**
 * List all get params and add to global variable
 * @param string|null $var
 * @return false|mixed|object
 */
function get(?string $var = null) {
    return Helper::get_params('get', $var);
}
$GLOBALS['get'] = get();

/**
 * List all post params and add to global variable
 * @param string|null $var
 * @return false|mixed|object
 */
function post(?string $var = null) {
    return Helper::get_params('post', $var);
}
$GLOBALS['post'] = post();

/**
 * Get the storage path of a file
 * @param ...$path
 * @return string
 */
function file_path(...$path): string
{
    return Helper::file_path(...$path);
}

/**
 * Get the public url of a file
 * @param ...$path
 * @return string
 */
function public_url(...$path): string
{
    return Helper::public_url(...$path);
}

/**
 * Get an environment setting
 * @param string $key
 * @return string
 */
function env(string $key): string
{
    return Helper::get_env($key);
}
