<?php

namespace Imgnd\Arti;

use eftec\bladeone\BladeOne;
use Exception;

class View
{
    /**
     * @var BladeOne $blade
     */
    public static BladeOne $blade;

    /**
     * View constructor
     */
    public function __construct()
    {
        $views = file_path('views');
        $cache = file_path('views', 'cache');
        self::$blade = new BladeOne($views, $cache);
    }

    /**
     * Render a view template
     * @param string $view
     * @param array $params
     * @throws Exception
     */
    public static function render(string $view, array $params = [])
    {
        echo self::$blade->run($view, $params);
        exit;
    }

    /**
     * Get html of a view
     * @param string $view
     * @param array $params
     * @return string
     * @throws Exception
     */
    public static function get(string $view, array $params = []): string
    {
        return self::$blade->run($view, $params);
    }
}

/** Initialise View */
new View();