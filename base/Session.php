<?php

namespace Imgnd\Arti;

use Imgnd\Arti\Helper;

class Session
{
    public function __construct()
    {
        session_start();
        $_SESSION['id'] = session_id();
    }

    /**
     * Get session params as value or all as object
     * @param string|null $var
     * @return mixed|object|false
     */
    public static function get_params(?string $var = null)
    {
        return Helper::get_params('session', $var, $_SESSION);
    }

    /**
     * Add or update a session value
     * @param string $key
     * @param mixed $value
     * @return object
     */
    public static function set(string $key, $value): object
    {
        $_SESSION[$key] = $value;
        return self::get_params();
    }

}

new Session();
$GLOBALS['session'] = Session::get_params();
