<?php

namespace Imgnd\Arti;

class Helper
{
    /**
     * Dump one or more vars of any type in nice pre tags
     * @param  ...$var
     * @return string
     */
    public static function pre_dump(...$var): string
    {
        ob_start();
        echo '<pre>';
        var_dump(...$var);
        echo '</pre>';
        return ob_get_clean();
    }

    /**
     * Echo pre_dump
     * @param ...$var
     */
    public static function dump(...$var): void
    {
        echo self::pre_dump(...$var);
    }

    /**
     * Get input params (get, post, cookie, session, array) as value or all as object
     * @param string $type
     * @param string|null $var
     * @param array $params
     * @return mixed|object|false
     */
    public static function get_params(string $type, ?string $var = null, array $params = [])
    {
        if (empty($params)) {
            $params = filter_input_array(constant(strtoupper('input_'.$type)));
        }
        if (isset($params[$var])) {
            return $params[$var];
        }
        if (!empty($params) && empty($var)) {
            $list = [];
            foreach ($params as $key => $val) {
                if (is_string($val)) {
                    $list[$key] = htmlentities($val);
                } else {
                    $list[$key] = $val;
                }
            }
            return (object) $list;
        }
        return false;
    }

    /**
     * Get the storage path of a file
     * @param ...$path
     * @return string
     */
    public static function file_path(...$path): string
    {
        array_unshift($path, dirname(__DIR__,4));
        return implode(DS, $path);
    }

    /**
     * Get the public url of a file
     * @param ...$path
     * @return string
     */
    public static function public_url(...$path): string
    {
        $version = null;
        if ($path[count($path)-1] === true) {
            array_pop($path);
            $file = self::file_path(...$path);
            if (file_exists($file)) {
                $version = '?v=' . @filemtime($file);
            }
        }
        if(WPMU_PLUGIN_URL !== null) {
            array_unshift($path, WPMU_PLUGIN_URL);
        } else {
            array_unshift($path, (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://'.$_SERVER['HTTP_HOST']);
        }
        return implode('/', $path) . $version;
    }

    /**
     * Assign .env items
     */
    public static function assign_env()
    {
        $file = Helper::file_path('.env');
        if (file_exists($file)) {
            $file = file_get_contents($file);
            foreach (explode("\n", $file) as $assignment) {
                if (!empty($assignment)) {
                    putenv($assignment);
                }
            }
        }
    }

    /**
     * Get an environment setting
     * @param string $key
     * @return string
     */
    public static function get_env(string $key): string
    {
        return trim(getenv($key), '"');
    }
}
