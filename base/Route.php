<?php

namespace Imgnd\Arti;

use Exception;
use ReflectionMethod;
use ReflectionException;

class Route
{
    public static array $routes = [];
    public static array $params = [];

    /**
     * Route constructor
     * @throws Exception
     */
    public function __construct()
    {
        $files = glob(file_path('routes/*.php'));
        foreach ($files as $file) {
            if (file_exists($file)) {
                include_once($file);
            }

            list($request) = explode('?', $_SERVER['REQUEST_URI'], 2);

            foreach (self::$routes as $uri => $action) {
                $parts = preg_split('/{[^}]*}/', $uri);
                /** Match full uri first **/
                if (count($parts) === 1) {
                    if ($uri == $request) {
                        if ($this->callMethod($action, $this->getParams($uri, $request))) {
                            return;
                        }
                    }
                    continue;
                }
                /** Match all uri parts */
                if (substr_count($request, '/') === substr_count($uri, '/')) {
                    foreach ($parts as $part) {
                        if (!empty($part)) {
                            if (strpos($request, $part) === false) {
                                continue 2;
                            }
                        }
                    }
                    if ($this->callMethod($action, $this->getParams($uri, $request))) {
                        return;
                    }
                }
            }
        }
        /** Catch 404 when no route found and routes are set */
        if (!empty(self::$routes) && file_exists(file_path('views', '404.blade.php'))) {
            header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
            View::render('404');
        }
    }

    /**
     * Link all urls to actions
     * @param string $uri
     * @param callable|string $action
     * @param array $params
     */
    public static function link(string $uri, $action, array $params = []): void
    {
        self::$routes[$uri] = $action;
        self::$params[$uri] = $params;
    }

    /**
     * Execute the action
     * @param $action
     * @param array|null $params
     * @return bool
     * @throws ReflectionException
     */
    private function callMethod($action, ?array $params = null): bool
    {
        $static = true;
        if (is_string($action)) {
            $action = explode('@', $action, 2);
        }
        if (is_array($action)) {
            $method = new ReflectionMethod(...$action);
            $static = $method->isStatic();
        }
        if (is_callable($action) && $static) {
            $action(...$params);
            return true;
        }
        list($class, $method) = $action;
        if (class_exists($class) && method_exists($class, $method)) {
            (new $class)->$method(...$params);
            return true;
        }
        return false;
    }

    /**
     * Get the parameter values from the url
     * @param string $uri
     * @param string $request
     * @return array
     */
    private function setParams(string $uri, string $request): array
    {
        $one = explode('/', $uri);
        $two = explode('/', $request);
        return array_diff($two, $one);
    }


    private function getParams(string $uri, string $request): array
    {
        return array_merge(self::$params[$uri], $this->setParams($uri, $request));
    }
}

new Route();